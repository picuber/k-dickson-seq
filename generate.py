import os
import sys
from itertools import product


def condition3(vs, v2):
    """
    Test for condition 3:
    "There are no 1 <= i < j <= K such that v_i <= v_j
    """

    return not any(all(n1 <= n2 for n1, n2 in zip(v1, v2)) for v1 in vs)


def deltas(dim):
    """
    Generate all allowed deltas of a vector, according to
    conditions 2.1 and 2.2 .
    """

    # all deltas such that they conform to condition 2.1
    delta_2_1 = product([1, 0, -1], repeat=dim)

    # only keep those that also conform to condition 2.2
    delta_2_2 = filter(lambda x: abs(sum(x)) <= 1, delta_2_1)
    return list(delta_2_2)


def start_vector(dim):
    """
    Get the starting vector conforming to condition 1
    """

    return tuple([1] + [0] * (dim - 1))


def apply_delta(vector, delta):
    """
    Apply a delta to a vector.
    Add each vector componentwise.
    """
    return tuple(map(sum, zip(vector, delta)))


def generate(dim, *, max_step=0, modulo=0):
    """
    Generate all the vectors you can find by recursively applying the deltas in
    a DFS.  For each found vector check if condition 3 holds and only continue
    with it if it does. Keep the longest sequence leading to this vector
    (= "history").

    Parameters
    ----------
    dim : int
        Dimension of the K-Dickson Sequence
    steps : int
        How many iterations should be done before terminating the search.
        If set to 0, run indefinetly
    modulo : int
        After every `modulo` steps, output the currently longest sequence.
        If set to 0, do not flush intermediate results.

    Return
    ------
        - The generated map of vectors to their longest sequence leading to it.
        - The number of steps done
    """

    delta_list = deltas(dim)
    found = {}   # vector -> (seq_length, history)
    step = 1   # step how many iterations we do
    dfs = []    # DFS-Stack (current vector, seq_length, history)

    dfs.append((start_vector(dim), 1, []))
    while (len(dfs)):
        print(f"step {step}", end='\r')
        vector, length, history = dfs.pop()

        # If we already have found a longer history for this vector, discard
        if vector in found and found[vector][0] >= length:
            continue

        found[vector] = (length, history)

        for delta in delta_list:
            new_vector = apply_delta(vector, delta)

            # Vectors are not allowed to have negative numbers
            # The only possible negative number is -1 because of condition 2.1
            if -1 in new_vector:
                continue

            new_history = history + [vector]
            if new_vector not in found or found[new_vector][0] < length + 1:
                if condition3(new_history, new_vector):
                    dfs.append((new_vector, length + 1, new_history))

        if modulo and step % modulo == 0:
            flush(found, step, dim)

        if max_step and step >= max_step:
            break

        step += 1
    return found, step


def flush(found, step, dim):
    """
    Print the (intermediate) result and write it to file.
    """
    length, sequence = longest(found, dim)
    result = format_result(sequence)
    write_to_file(length, result, dim)
    print(result)
    print(f"#####{length}@{step}#####")


def longest(found, dim):
    sequence = [start_vector(dim)]
    max_length = 1

    for vector, (length, history) in found.items():
        if length > max_length:
            max_length = length
            sequence = history + [vector]

    return max_length, sequence


def format_result(seq):
    out = []
    for v in seq:
        out.append(','.join([str(i) for i in v]))
    return '\n'.join(out)


def write_to_file(length, result, dim):
    dirname = "dim" + str(dim)
    if not os.path.exists(dirname):
        os.mkdir(dirname)
    f = open(dirname + '/' + str(length) + '.txt', 'w')
    f.write(result)
    f.close()


def usage():
    out = f"Usage: python3 {sys.argv[0]} <dimension> [<max_step> [<modulo>]]\n"
    out += "    - dimension (int): dimension of the Dickson sequence\n"
    out += "    - max_step (int): do a maximum of <max_step> iterations of"
    out += " DFS.\n"
    out += "        (default = 0, do not limit the number of iterations)\n"
    out += "    - modulo (int): generate an intermediate result every <modulo>"
    out += " steps.\n"
    out += "        (default = 0, do not generate intermediate results)"
    print(out)


if __name__ == "__main__":
    dim = 5
    max_step = 0
    modulo = 0
    if len(sys.argv) == 1:
        usage()
        sys.exit(0)
    if len(sys.argv) > 1:
        dim = int(sys.argv[1])
    if len(sys.argv) > 2:
        max_step = int(sys.argv[2])
    if len(sys.argv) > 3:
        modulo = int(sys.argv[3])

    found, step = generate(dim, max_step=max_step, modulo=modulo)
    flush(found, step, dim)
