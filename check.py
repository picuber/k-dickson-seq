#!/usr/bin/env python3

import sys
import fileinput


def condition1(v):
    return len(v) > 0 and v[0] == 1 and all(n == 0 for n in v[1:])


def condition2_1(v1, v2):
    return max((abs(n1 - n2) for n1, n2 in zip(v1, v2))) <= 1


def condition2_2(v1, v2):
    return abs(sum(v1) - sum(v2)) <= 1


def condition3(vs, v2):
    return not any(all(n1 <= n2 for n1, n2 in zip(v1, v2)) for v1 in vs)


def check_conditions(vecs):
    if len(vecs) == 0:
        raise Exception(f"Error: empty sequence")

    if not condition1(vecs[0]):
        raise Exception(f"Error: invalid first vector: {vecs[0]}")
    for i, v in enumerate(vecs[1:], 1):
        if not condition2_1(vecs[i - 1], v):
            raise Exception(f"Error: invalid vector at position {i}: element"
                            " differs by more than one from predecessor")
        if not condition2_2(vecs[i - 1], v):
            raise Exception(f"Error: invalid vector at position {i}: sum"
                            " differs by more than one from predecessor")
        if not condition3(vecs[:i], v):
            raise Exception(f"Error: invalid vector at position {i}: covers"
                            " previous element")


def parse_input():
    k = 0
    vecs = []
    for i, line in enumerate(fileinput.input(), 1):
        ls = line.strip()
        if not ls:
            raise Exception(f"Error: line {i} is empty")
        v = []
        for e in ls.split(","):
            try:
                n = int(e.strip())
                if n < 0:
                    raise Exception(f"Error: line {i} contains a negative"
                                    f" integer: {line}")
                v.append(n)
            except ValueError:
                raise Exception(f"Error: line {i} contains a"
                                f" non-integer: {line}")
        if i == 1:
            k = len(v)
        else:
            if k != len(v):
                raise Exception(f"Error: line {i} has a vector with"
                                f" mismatched dimensions: {line}")
        vecs.append(v)
    return vecs


def check():
    vecs = parse_input()
    check_conditions(vecs)
    return len(vecs), len(vecs[0])


if __name__ == '__main__':
    try:
        n, k = check()
        print(f"Valid sequence of length {n} and dimension {k}")
    except Exception as e:
        print(e)
        sys.exit(1)
